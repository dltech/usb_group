#ifndef H_SFR
#define H_SFR
/*
 *  CC1110 USB UHF stick. Special function (peripherial); registers.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# define SFR(name, addr)        __sfr   __at(addr)                        name
# define SFRX(name, addr)       __xdata volatile unsigned char __at(addr) name

#define F_SYSTEM    26000000
#define F_REF       F_SYSTEM

//
#define

/* interrupt vectors */
#define RFTXRX_INT  0   // RF TX done / RX ready
#define ADC_INT     1   // ADC end of conversion
#define URX0_INT    2   // RUSART0 RX complete
#define URX1_INT    3   // USART1 RX complete
#define ENC_INT     4   // AES encryption/decryption complete
#define ST_INT      5   // Sleep Timer compare
#define P2INT_INT   6   // Port 2 inputs
#define UTX0_INT    7   // USART0 TX complete
#define DMA_INT     8   // DMA transfer complete
#define T1_INT      9   // Timer 1 (16-bit) capture/Compare/overflow
#define T2_INT      10  // Timer 2 (MAC Timer) overflow
#define T3_INT      11  // Timer 3 (8-bit) compare/overflow
#define T4_INT      12  // Timer 4 (8-bit) compare/overflow
#define P0INT_INT   13  // Port 0 inputs
#define UTX1_INT    14  // USART1 TX complete
#define P1INT_INT   15  // Port 1 inputs
#define RF_INT      16  // RF general interrupts
#define WDT_INT     17  // Watchdog overflow in timer mode

SFR(IEN0, 0xa8); // Interrupt Enable 0 Register
// Enable All
#define EA          0x80
// Sleep Timer interrupt enable
#define STIE        0x20
// AES encryption/decryption interrupt enable
#define ENCIE       0x10
// USART1 RX interrupt enable / I2S RX interrupt enable
#define URX1IE      0x08
#define I2SRXIE     0x08
// USART0 RX interrupt enable
#define URX0IE      0x04
// ADC interrupt enable
#define ADCIE       0x02
// RF TX/RX done interrupt enable
#define RFTXRXIE    0x01
SFR(IEN1, 0xb8); // Interrupt Enable 1 Register
// Port 0 interrupt enable
#define P0IE    0x20
// Timer 4 interrupt enable
#define T4IE    0x10
// Timer 3 interrupt enable
#define T3IE    0x08
// Timer 2 interrupt enable
#define T2IE    0x04
// Timer 1 interrupt enable
#define T1IE    0x02
// DMA transfer interrupt enable
#define DMAIE   0x01
SFR(IEN2, 0x9a); // Interrupt Enable 2 Register
// Watchdog timer interrupt enable
#define WDTIE   0x20
// Port 1 interrupt enable
#define P1IE    0x10
// USART1 TX interrupt enable / I2S TX interrupt enable
#define UTX1IE  0x08
#define I2STXIE 0x08
// USART0 TX interrupt enable
#define UTX0IE  0x04
// Port 2 interrupt enable (Also used for USB interrupt enable on CC1111Fx)
#define P2IE    0x02
#define USBIE   0x02
// RF general interrupt enable
#define RFIE    0x01

SFR(TCON, 0x88); // CPU Interrupt Flag 1
// USART1 RX interrupt flag / I2S RX interrupt flag
#define URX1IF      0x80
#define I2SRXIF     0x80
// ADC interrupt flag.
#define ADCIF       0x20
// USART0 RX interrupt flag.
#define URX0IF      0x08
// RF TX/RX complete interrupt flag.
#define RFTXRXIF    0x02
SFR(S0CON, 0x98); // CPU Interrupt Flag 2
// AES interrupt.
#define ENCIF_1 0x02
// AES interrupt.
#define ENCIF_0 0x01
SFR(S1CON, 0x9b); // CPU Interrupt Flag 3
// RF general interrupt.
#define RFIF_1  0x02
// RF general interrupt.
#define RFIF_0  0x01
SFR(IRCON, 0xc0); // CPU Interrupt Flag 4
// Sleep Timer interrupt flag
#define STIF    0x80
// Port 0 interrupt flag.
#define P0IF    0x20
// Timer 4 interrupt flag.
#define T4IF    0x10
// Timer 3 interrupt flag.
#define T3IF    0x08
// Timer 2 interrupt flag.
#define T2IF    0x04
// Timer 1 interrupt flag.
#define T1IF    0x02
// DMA complete interrupt flag.
#define DMAIF   0x01
SFR(IRCON2, 0xe8); // CPU Interrupt Flag 5
// Sleep Timer interrupt flag
#define STIF    0x80
// Port 0 interrupt flag.
#define P0IF    0x20
// Timer 4 interrupt flag.
#define T4IF    0x10
// Timer 3 interrupt flag.
#define T3IF    0x08
// Timer 2 interrupt flag.
#define T2IF    0x04
// Timer 1 interrupt flag.
#define T1IF    0x02
// DMA complete interrupt flag.
#define DMAIF   0x01

SFR(IP1, 0xb9); // Interrupt Priority 1
// Interrupt group x, priority control bit 1
#define IP1_IPG5    0x20
#define IP1_IPG4    0x10
#define IP1_IPG3    0x08
#define IP1_IPG2    0x04
#define IP1_IPG1    0x02
#define IP1_IPG0    0x01
#define IP1_IPGx(n) (1<<n)
SFR(IP0, 0xa9); // Interrupt Priority 0
// Interrupt group x, priority control bit 0
#define IP0_IPG5    0x20
#define IP0_IPG4    0x10
#define IP0_IPG3    0x08
#define IP0_IPG2    0x04
#define IP0_IPG1    0x02
#define IP0_IPG0    0x01
#define IP0_IPGx(n) (1<<n)

SFR(PCON, 0x87); // Power Mode Control
// set the sleep mode
#define IDLE    0x1
SFR(SLEEP, 0xbe); // Sleep Mode Control
// USB Enable
#define USB_EN      0x80
// High speed crystal oscillator (fXOSC) stable status
#define XOSC_STB    0x40
// High speed RC oscillator (HS RCOSC) stable status
#define HFRC_STB    0x20
// Status bit indicating the cause of the last reset.
#define RST_POR     0x00
#define RST_EXT     0x08
#define RST_WDT     0x10
// High speed XOSC and HS RCOSC power down setting.
#define OSC_PD      0x04
// Power mode setting
#define MODE_PM0    0x00
#define MODE_PM1    0x01
#define MODE_PM2    0x02
#define MODE_PM3    0x03

SFR(CLKCON, 0xc6); // Clock Control
// 32 kHz clock oscillator select.
#define OSC32K          0x80
// System clock oscillator select.
#define OSC             0x40
// Timer tick speed setting.
#define TICKSPD26M      0x00
#define TICKSPD13M      0x08
#define TICKSPD6P5M     0x10
#define TICKSPD3P25M    0x18
#define TICKSPD1P6M     0x20
#define TICKSPD825K     0x28
#define TICKSPD406K     0x30
#define TICKSPD203K     0x38
// System clock speed setting.
#define CLKSPD26M       0x00
#define CLKSPD13M       0x01
#define CLKSPD6P5M      0x02
#define CLKSPD3P25M     0x03
#define CLKSPD1P6M      0x04
#define CLKSPD825K      0x05
#define CLKSPD406K      0x06
#define CLKSPD203K      0x07

/*** Flash module registers ***/
SFR(FWT, 0xab); /* Flash Write Timing */
#define FWT_CALC    ((21*F_SYSTEM)/16000000)
#define FWT_MSK     0x3f

SFR(FADDRL, 0xac);  /* Flash Address Low */
SFR(FADDRH, 0xad); /* Flash Address High */
#define FADDRH_MSK  0x3f

SFR(FCTL, 0xae); /* Flash Control */
// Indicates that write or erase is in operation when set to 1.
#define BUSY    0x80
// Indicates that a flash write is in progress.
#define SWBSY   0x40
// Continuous read enable
#define CONTRD  0x10
// initiat a command to write memory
#define WRITE   0x02
// Page Erase.
#define ERASE   0x01

SFR(FWDATA, 0xaf); /* Flash Write Data */

/*** ADC module registers ***/
SFR(ADCH, 0xbb);    /* ADC Data High */
SFR(ADCL, 0xba);    /* ADC Data Low */

SFR(ADCCON1, 0xb4); /* ADC Control 1 */
// End of conversion.
#define EOC     0x80
// Start conversion.
#define ST      0x40
// Start select.
#define STSEL_EXT   0x00
#define STSEL_FULL  0x10
#define STSEL_TIM1  0x20
#define STSEL_ST    0x30
// Controls the 16 bit random generator.
#define RCTRL_COMPL 0x00
#define RCTRL_ON    0x04
SFR(ADCCON2, 0xb5); /* ADC Control 2 */
// Selects reference voltage used for the sequence of conversions
#define SREF_INT      0x00
#define SREF_EXT      0x40
#define SREF_VDD      0x80
#define SREF_EXT_DIFF 0xc0
// Sets the decimation rate for channels included in the sequence.
#define SDIV64        0x00
#define SDIV128       0x10
#define SDIV256       0x20
#define SDIV512       0x30
// Sequence Channel Select.
#define SCH_AIN0      0x00
#define SCH_AIN1      0x01
#define SCH_AIN2      0x02
#define SCH_AIN3      0x03
#define SCH_AIN4      0x04
#define SCH_AIN5      0x05
#define SCH_AIN6      0x06
#define SCH_AIN7      0x07
#define SCH_AIN0_1    0x08
#define SCH_AIN2_3    0x09
#define SCH_AIN4_5    0x0a
#define SCH_AIN6_7    0x0b
#define SCH_GND       0x0c
#define SCH_REF       0x0d
#define SCH_TEMP      0x0e
#define SCH_VDD_DIV3  0x0f
SFR(ADCCON3, 0xb6); /* ADC Control 3 */
// Selects reference voltage used for the extra conversion
#define EREF_INT      0x00
#define EREF_EXT      0x40
#define EREF_VDD      0x80
#define EREF_EXT_DIFF 0xc0
// Sets the decimation rate used for the extra conversion.
#define EDIV64        0x00
#define EDIV128       0x10
#define EDIV256       0x20
#define EDIV512       0x30
// Extra channel select.
#define ECH_AIN0      0x00
#define ECH_AIN1      0x01
#define ECH_AIN2      0x02
#define ECH_AIN3      0x03
#define ECH_AIN4      0x04
#define ECH_AIN5      0x05
#define ECH_AIN6      0x06
#define ECH_AIN7      0x07
#define ECH_AIN0_1    0x08
#define ECH_AIN2_3    0x09
#define ECH_AIN4_5    0x0a
#define ECH_AIN6_7    0x0b
#define ECH_GND       0x0c
#define ECH_REF       0x0d
#define ECH_TEMP      0x0e
#define ECH_VDD_DIV3  0x0f

/*** Random Number Generator module registers ***/
SFR(RNDH, 0xbd);    /* Random Number Generator Data High */
SFR(RNDL, 0xbc);    /* Random Number Generator Data Low */

/*** AES Coprocessor registers ***/
SFR(ENCCS, 0xb3);   /* Encryption/Decryption Control and Status */
// Encryption/decryption mode
#define MODE_CBC        0x00
#define MODE_CFB        0x10
#define MODE_OFB        0x20
#define MODE_CTR        0x30
#define MODE_ECB        0x40
#define MODE_CBC_MAC    0x50
// Encryption/decryption ready status
#define RDY             0x08
// Command to be performed when a 1 is written to ST.
#define CMD_ENC         0x00
#define CMD_DEC         0x02
#define CMD_LOAD_KEY    0x04
#define CMD_LOAD_IV     0x06
// Start processing command set by CMD.
#define AES_ST          0x01
SFR(ENCDI, 0xb1);   /* Encryption/Decryption Input Data */
SFR(ENCDO, 0xb2);   /* Encryption/Decryption Output Data */

/*** Watchdog Timer register ***/
SFR(WDCTL, 0xc9); /* Watchdog Timer Control */
// Clear timer
#define CLR1    0xa0
#define CLR2    0x50
// Enable timer.
#define WD_EN   0x08
// Mode select.
#define WD_MODE 0x04
// Timer interval select.
#define WD_INT32k   0x00
#define WD_INT8k    0x01
#define WD_INT512   0x02
#define WD_INT64    0x03

/*** DMA module registers ***/
SFR(ENDIAN, 0x95); /* USB Endianess Control */
// USB Write Endianess setting for DMA channel word transfers to USB.
#define USBWLE  0x02
// USB Read Endianess setting for DMA channel word transfers from USB.
#define USBRLE  0x01
SFR(DMAIRQ, 0xd1); /* DMA Interrupt Flag */
// DMA channel 4 - 0 interrupt flag.
#define DMAIF4  0x10
#define DMAIF3  0x08
#define DMAIF2  0x04
#define DMAIF1  0x02
#define DMAIF0  0x01
SFR(DMA1CFGH, 0xd3); /* DMA Channel 1 - 4 Configuration Address High */
SFR(DMA1CFGL, 0xd2); /* DMA Channel 1 - 4 Configuration Address Low */
// DMA1CFG[15:8], DMA1CFG[7:0]
SFR(DMA0CFGH, 0xd5); /* DMA Channel 0 Configuration Address High */
SFR(DMA0CFGL, 0xd4); /* DMA Channel 0 Configuration Address Low */
// DMA0CFG[15:8], DMA0CFG[7:0]
SFR(DMAARM, 0xd6);   /* DMA Channel Arm */
// DMA abort.
#define ABORT   0x80
// DMA arm channel 4
#define DMAARM4 0x10
// DMA arm channel 3
#define DMAARM3 0x08
// DMA arm channel 2
#define DMAARM2 0x04
// DMA arm channel 1
#define DMAARM1 0x02
// DMA arm channel 0
#define DMAARM0 0x01
SFR(DMAREQ, 0xd7);   /* DMA Channel Start Request and Status */
// DMA channel 4, manual trigger
#define DMAREQ4 0x10
// DMA channel 3, manual trigger
#define DMAREQ3 0x08
// DMA channel 2, manual trigger
#define DMAREQ2 0x04
// DMA channel 1, manual trigger
#define DMAREQ1 0x02
// DMA channel 0, manual trigger
#define DMAREQ0 0x01

/*** IOC module registers ***/
SFR(P0, 0x80);  /* Port 0 */
#define GPIO0   0x01
#define GPIO1   0x02
#define GPIO2   0x04
#define GPIO3   0x08
#define GPIO4   0x10
#define GPIO5   0x20
#define GPIO6   0x40
#define GPIO7   0x80
SFR(P1, 0x90);  /* Port 1 */
SFR(P2, 0xa0);  /* Port 2 */

SFR(P0IFG, 0x89); /* Port 0 Interrupt Status Flag */
// USB resume detected during suspend mode
#define USB_RESUME  0x80
// Port 0, inputs 7 to 0 interrupt status flags.
#define P0IF0   0x01
#define P0IF1   0x02
#define P0IF2   0x04
#define P0IF3   0x08
#define P0IF4   0x10
#define P0IF5   0x20
#define P0IF6   0x40
#define P0IF7   0x80
SFR(P1IFG, 0x8a); /* Port 1 Interrupt Status Flag */
// Port 1, inputs 7 to 0 interrupt status flags.
#define P1IF0   0x01
#define P1IF1   0x02
#define P1IF2   0x04
#define P1IF3   0x08
#define P1IF4   0x10
#define P1IF5   0x20
#define P1IF6   0x40
#define P1IF7   0x80
SFR(P2IFG, 0x8b); /* Port 2 Interrupt Status Flag */
// Port 2, inputs 4 to 0 interrupt status flags.
#define P2IF0   0x01
#define P2IF1   0x02
#define P2IF2   0x04
#define P2IF3   0x08
#define P2IF4   0x10

SFR(PICTL, 0x8c); /* Port Pins Interrupt Mask and Edge */
// Port 2, inputs 4 to 0 interrupt enable.
#define P2IEN   0x20
// Port 0, inputs 7 to 4 interrupt enable.
#define P0IENH  0x10
// Port 0, inputs 3 to 0 interrupt enable.
#define P0IENL  0x08
// Port 2, inputs 4 to 0 interrupt configuration.
#define P2ICON  0x04
// Port 1, inputs 7 to 0 interrupt configuration.
#define P1ICON  0x02
// Port 0, inputs 7 to 0 interrupt configuration.
#define P0ICON  0x01
SFR(P1IEN, 0x8d); /* Port 1 Interrupt Mask */
// Port P1_7 to P1_0 interrupt enable
#define P1_0IEN 0x01
#define P1_1IEN 0x02
#define P1_2IEN 0x04
#define P1_3IEN 0x08
#define P1_4IEN 0x10
#define P1_5IEN 0x20
#define P1_6IEN 0x40
#define P1_7IEN 0x80

SFR(PERCFG, 0xf1); /* Peripheral I/O Control */
// Timer 1 I/O location
#define T1CFG   0x40
// Timer 3 I/O location
#define T3CFG   0x20
// Timer 4 I/O location
#define T4CFG   0x10
// USART1 I/O location
#define U1CFG   0x02
// USART0 I/O location
#define U0CFG   0x01
SFR(ADCCFG, 0xf2); /* ADC Input Configuration */
// select P0_7 - P0_0 as ADC inputs AIN7 - AIN0
#define ADCCFG_AIN0 0x01
#define ADCCFG_AIN1 0x02
#define ADCCFG_AIN2 0x04
#define ADCCFG_AIN3 0x08
#define ADCCFG_AIN4 0x10
#define ADCCFG_AIN5 0x20
#define ADCCFG_AIN6 0x40
#define ADCCFG_AIN7 0x80

SFR(P0SEL, 0xf3); /* Port 0 Function Select */
// P0_7 to P0_0 function select
#define SELP0_0 0x01
#define SELP0_1 0x02
#define SELP0_2 0x04
#define SELP0_3 0x08
#define SELP0_4 0x10
#define SELP0_5 0x20
#define SELP0_6 0x40
#define SELP0_7 0x80
SFR(P1SEL, 0xf4); /* Port 1 Function Select */
// P1_7 to P1_0 function select
#define SELP1_0 0x01
#define SELP1_1 0x02
#define SELP1_2 0x04
#define SELP1_3 0x08
#define SELP1_4 0x10
#define SELP1_5 0x20
#define SELP1_6 0x40
#define SELP1_7 0x80
SFR(P2SEL, 0xf5); /* Port 2 Function Select */
// 0 - USART0 has priority, 1 - USART1 has priority
#define PRI3P1  0x40
// 0 - USART1 has priority, 1 - Timer 3 has priority
#define PRI2P1  0x20
// 0 - Timer 1 has priority, 1 - Timer 4 has priority
#define PRI1P1  0x10
// 0 - USART0 has priority, 1 - Timer 1 has priority
#define PRI0P1  0x08
// P2_4 to P2_0 function select
#define SELP2_0 0x01
#define SELP2_3 0x02
#define SELP2_4 0x04

SFR(P0INP, 0x8f);   /* Port 0 Input Mode */
// P0_7 to P0_0 I/O input mode
#define MDP0_0  0x01
#define MDP0_1  0x02
#define MDP0_2  0x04
#define MDP0_3  0x08
#define MDP0_4  0x10
#define MDP0_5  0x20
#define MDP0_6  0x40
#define MDP0_7  0x80
SFR(P1INP, 0xf6);   /* Port 1 Input Mode */
// P1_7 to P1_2 I/O input mode
#define MDP1_2  0x04
#define MDP1_3  0x08
#define MDP1_4  0x10
#define MDP1_5  0x20
#define MDP1_6  0x40
#define MDP1_7  0x80
SFR(P2INP, 0xf7);   /* Port 2 Input Mode */
// Port 2 pull-up/down select.
#define PDUP2   0x80
// Port 1 pull-up/down select.
#define PDUP1   0x40
// Port 0 pull-up/down select.
#define PDUP0   0x20
// P2_4 to P2_0 I/O input mode
#define MDP2_0  0x01
#define MDP2_1  0x02
#define MDP2_2  0x04
#define MDP2_3  0x08
#define MDP2_4  0x10

SFR(P0DIR, 0xfd); /* Port 0 Direction */
// P0_7 to P0_0 I/O direction
#define DIRP0_0 0x01
#define DIRP0_1 0x02
#define DIRP0_2 0x04
#define DIRP0_3 0x08
#define DIRP0_4 0x10
#define DIRP0_5 0x20
#define DIRP0_6 0x40
#define DIRP0_7 0x80
SFR(P1DIR, 0xfe); /* Port 1 Direction */
// P1_7 to P1_0 I/O direction
#define DIRP1_0 0x01
#define DIRP1_1 0x02
#define DIRP1_2 0x04
#define DIRP1_3 0x08
#define DIRP1_4 0x10
#define DIRP1_5 0x20
#define DIRP1_6 0x40
#define DIRP1_7 0x80
SFR(P2DIR, 0xff); /* Port 2 Direction */
// Port 0 peripheral priority control.
#define PRIP0_USART0_USART1     0x00
#define PRIP0_USART1_USART0     0x40
#define PRIP0_TIM1_CH0_1_USART1 0x00
#define PRIP0_TIM1_CH2_USART0   0x00
// P2_4 to P2_0 I/O direction
#define DIRP2_0 0x01
#define DIRP2_1 0x02
#define DIRP2_2 0x04
#define DIRP2_3 0x08
#define DIRP2_4 0x10

/*** MEMORY module register ***/

/* Memory System Control */
SFR(MEMCTR, 0xc7);
// Flash cache disable.
#define CACHDIS 0x02
// Flash prefetch disable.
#define PREFDIS 0x01


/*** PMC module registers ***/

/* Sleep Mode Control */
SFR(SLEEP, 0xbe);

/* Clock Control */
SFR(CLKCON, 0xc6);


/*** RF module registers ***/

/* RF Interrupt Mask */
SFR(RFIM, 0x91);

/* RF Data */
SFR(RFD, 0xd9);

/* RF Interrupt flags */
SFR(RFIF, 0xe9);

/* RF Strobe Commands */
SFR(RFST, 0xe1);

/*** Sleep Timer module registers ***/
SFR(WORTIME1, 0xa6); /* Sleep Timer High Byte */
SFR(WORTIME0, 0xa4); /* Sleep Timer Low Byte */

SFR(WOREVT1, 0xa5);  /* Sleep Timer Event 0 Timeout High Byte */
SFR(WOREVT0, 0xa3);  /* Sleep Timer Event 0 Timeout Low Byte */

SFR(WORCTRL, 0xa2);  /* Sleep Timer Control */
// Reset timer. The timer will be reset to 4.
#define WOR_RESET           0x04
// Sleep Timer resolution
#define WOR_RES_1PERIOD     0x00
#define WOR_RES_2P5PERIOD   0x01
#define WOR_RES_2P10PERIOD  0x02
#define WOR_RES_2P15PERIOD  0x03
SFR(WORIRQ, 0xa1);   /* Sleep Timer Interrupts */
// Event 0 interrupt mask
#define EVENT0_MASK 0x10
// Event 0 interrupt flag
#define EVENT0_FLAG 0x01

/*** Timer1 module registers ***/
SFR(T1CNTH, 0xe3); /* Timer 1 Counter High */
SFR(T1CNTL, 0xe2); /* Timer 1 Counter Low */

SFR(T1CTL, 0xe4); /* Timer 1 Control and Status */
// Timer 1 channel 2 interrupt flag
#define CH2IF           0x80
// Timer 1 channel 1 interrupt flag
#define CH1IF           0x40
// Timer 1 channel 0 interrupt flag
#define CH0IF           0x20
// Timer 1 counter overflow interrupt flag.
#define OVFIF           0x10
// Prescaler divider value.
#define DIV_1           0x00
#define DIV_8           0x04
#define DIV_32          0x08
#define DIV_128         0x0c
// Timer 1 mode select.
#define MODE1_SUSPEND    0x00
#define MODE1_FREE       0x01
#define MODE1_MODULO     0x02
#define MODE1_UP_DOWN    0x03

SFR(T1CCTL0, 0xe5); /* Timer 1 Channel 0 Capture/Compare Control */
// Use RF event(s) enabled in the RFIM register to trigger a capture
#define CPSEL   0x80
// Channel 0 interrupt mask
#define IM      0x40
// Channel 0 compare mode select.
#define CMP_SET             0x00
#define CMP_CLEAR           0x08
#define CMP_TOGGLE          0x10
#define CMP_CLEAR_ON_DOWN   0x18
#define CMP_SET_ON_DOWN     0x20
#define CMP_CLEAR_ON_CC0    0x28
#define CMP_SET_ON_CC0      0x30
#define CMP_DSM             0x38
// Select Timer 1 channel 0 capture or compare mode
#define MODE                0x04
// Channel 0 capture mode select
#define CAP_NO              0x00
#define CAP_RISING_EDGE     0x01
#define CAP_FALLING_EGDE    0x02
#define CAP_BOTH_EDGES      0x03
SFR(T1CC0H, 0xdb); /* Timer 1 Channel 0 Capture/Compare Value High */
SFR(T1CC0L, 0xda); /* Timer 1 Channel 0 Capture/Compare Value Low */

SFR(T1CCTL1, 0xe6); /* Timer 1 Channel 1 Capture/Compare Control */
SFR(T1CC1H, 0xdd); /* Timer 1 Channel 1 Capture/Compare Value High */
SFR(T1CC1L, 0xdc); /* Timer 1 Channel 1 Capture/Compare Value Low */

SFR(T1CCTL2, 0xe7); /* Timer 1 Channel 2 Capture/Compare Control */
SFR(T1CC2H, 0xdf); /* Timer 1 Channel 2 Capture/Compare Value High */
SFR(T1CC2L, 0xde); /* Timer 1 Channel 2 Capture/Compare Value Low */

/*** Timer2 module registers ***/
SFR(T2CT, 0x9c); /* Timer 2 Timer Count */
SFR(T2PR, 0x9d); /* Timer 2 Prescaler */
SFR(T2CTL, 0x9e); /* Timer 2 Control */
// This bit is set to 1 when the timer count reaches 0x00.
#define TEX         0x40
// Timer 2 Interrupt enable
#define INT         0x10
// Tick generator mode
#define TIG         0x04
// This value is used to calculate the timer 2 interval / time slot
#define TIP_64      0x00
#define TIP_128     0x01
#define TIP_256     0x02
#define TIP_1024    0x03

/*** Timer3 module registers ***/
SFR(T3CNT, 0xca);   /* Timer 3 Counter */
SFR(T3CTL, 0xcb);   /* Timer 3 Control */
// Prescaler divider value.
#define DIV1    0x00
#define DIV2    0x20
#define DIV4    0x40
#define DIV8    0x60
#define DIV16   0x80
#define DIV32   0xa0
#define DIV64   0xc0
#define DIV128  0xe0
// Start timer
#define START           0x10
// Overflow interrupt mask
#define OVFIM           0x08
// Clear counter.
#define CLR             0x04
// Timer 3 mode select.
#define MODE3_FREE      0x00
#define MODE3_DOWN      0x01
#define MODE3_MODULO    0x02
#define MODE3_UP_DOWN   0x03

SFR(T3CCTL0, 0xcc); /* Timer 3 Channel 0 Capture/Compare Control */
// Channel 0 interrupt mask
// Channel 0 compare mode select.
// Select Timer 1 channel 0 capture or compare mode
SFR(T3CC0, 0xcd);   /* Timer 3 Channel 0 Capture/Compare Value */

SFR(T3CCTL1, 0xce); /* Timer 3 Channel 1 Capture/Compare Control */
SFR(T3CC1, 0xcf);   /* Timer 3 Channel 1 Capture/Compare Value */

/*** Timer4 module registers ***/
SFR(T4CNT, 0xea);   /* Timer 4 Counter */
SFR(T4CTL, 0xeb);   /* Timer 4 Control */
// Timer 4 mode select.
#define MODE4_FREE      0x00
#define MODE4_DOWN      0x01
#define MODE4_MODULO    0x02
#define MODE4_UP_DOWN   0x03

SFR(T4CCTL0, 0xec); /* Timer 4 Channel 0 Capture/Compare Control */
SFR(T4CC0, 0xed);   /* Timer 4 Channel 0 Capture/Compare Value */

SFR(T4CCTL1, 0xee); /* Timer 4 Channel 1 Capture/Compare Control */
SFR(T4CC1, 0xef);   /* Timer 4 Channel 1 Capture/Compare Value */

SFR(TIMIF, 0xd8);   /* Timers 1/3/4 Joint Interrupt Mask/Flags */
// Timer 1 overflow interrupt mask
#define OVFIM       0x40
// Timer 4 channel 1 interrupt flag. Writing a 1 has no effect
#define T4CH1IF     0x20
// Timer 4 channel 0 interrupt flag. Writing a 1 has no effect
#define T4CH0IF     0x10
// Timer 4 overflow interrupt flag. Writing a 1 has no effect
#define T4OVFIF     0x08
// Timer 3 channel 1 interrupt flag. Writing a 1 has no effect
#define T3CH1IF     0x04
// Timer 3 channel 0 interrupt flag. Writing a 1 has no effect
#define T3CH0IF     0x02
// Timer 3 overflow interrupt flag. Writing a 1 has no effect
#define T3OVFIF     0x01

/*** USART0 module registers ***/
SFR(U0CSR, 0x86);   /* USART 0 Control and Status */
// USART 0 mode select
#define USART_MODE  0x80
// UART 0 receiver enable
#define RE          0x40
// SPI 0 master or slave mode select
#define SLAVE       0x20
// UART 0 framing error status
#define FE          0x10
// UART 0 parity error status
#define ERR         0x08
// Receive byte status
#define RX_BYTE     0x04
// Transmit byte status
#define TX_BYTE     0x02
// USART 0 transmit/receive active status
#define ACTIVE      0x01
SFR(U0UCR, 0xc4);   /* USART 0 UART Control */
// Flush unit.
#define FLUSH       0x80
// UART 0 hardware flow control enable.
#define FLOW        0x40
// UART 0 data bit 9 contents.
#define D9          0x20
// UART 0 9-bit data enable
#define BIT9        0x10
// UART 0 parity enable
#define PARITY      0x08
// UART 0 number of stop bits
#define SPB         0x04
// UART 0 stop bit level
#define STOP        0x02
// UART 0 start bit level.
#define START       0x01
SFR(U0GCR, 0xc5);   /* USART 0 Generic Control */
// SPI 0 clock polarity
#define CPOL        0x80
// SPI 0 clock phase
#define CPHA        0x40
// Bit order for transfers
#define ORDER       0x20
// Baud rate exponent value.
#define BAUD_E_MSK  0x1f
SFR(U0BAUD, 0xc2);  /* USART 0 Baud Rate Control */
SFR(U0DBUF, 0xc1);  /* USART 0 Receive/Transmit Data Buffer */

/*** USART1 module registers ***/
SFR(U1CSR, 0xf8);  /* USART 1 Control and Status */
SFR(U1UCR, 0xfb);  /* USART 1 UART Control */
SFR(U1GCR, 0xfc);  /* USART 1 Generic Control */
SFR(U1BAUD, 0xfa); /* USART 1 Baud Rate Control */
SFR(U1DBUF, 0xf8); /* USART 1 Receive/Transmit Data Buffer */

SFR(ENDIAN, 0x95); /* USB Endianess Control (CC1111Fx) */

/*** I2S Registers ***/
SFRX(I2SCFG0, 0xdf40);  /* I2S Configuration Register 0 */
// Transmit interrupt enable
#define TXIEN   0x80
// Receive interrupt enable
#define RXIEN   0x40
// μ-Law expansion enable
#define ULAWE   0x20
// μ-Law compression enable
#define ULAWC   0x10
// TX mono enable
#define TXMONO  0x08
// RX mono enable
#define RXMONO  0x04
// Master mode enable
#define MASTER  0x02
// I2S interface enable
#define ENAB    0x01
SFRX(I2SCFG1, 0xdf41);  /* I2S Configuration Register 1 */
// This field gives the word size – 1.
#define WORDS_MSK   0xf8
#define WORDS_SFT   3
// Word counter copy / clear trigger
#define TRIGNUM_NO      0x00
#define TRIGNUM_SOF     0x02
#define TRIGNUM_IOC1    0x04
#define TRIGNUM_T1_CH0  0x06
// The pin locations for the I2S signals.
#define IOLOC           0x01

SFRX(I2SDATH, 0xdf43);  /* I2S Data High Byte */
SFRX(I2SDATL, 0xdf42);  /* I2S Data Low Byte */

SFRX(I2SWCNT, 0xdf44);  /* I2S Word Count Register */

SFRX(I2SSTAT, 0xdf45);  /* I2S Status Register */
// TX buffer underflow.
#define TXUNF       0x80
// Rx buffer overflow.
#define RXOVF       0x40
// Left/Right channel should be placed in transmit buffer
#define TXLR        0x20
// Left/Right channel currently in receive buffer
#define RXLR        0x10
// TX interrupt flag.
#define TXIRQ       0x08
// RX Interrupt flag.
#define RXIRQ       0x04
// Upper 2 bits of the 10-bit internal word counter
#define WCNT_MSK    0x03

SFRX(I2SCLKF0, 0xdf46); /* I2S Clock Configuration Register 0 */
//DENOM[7:0]
SFRX(I2SCLKF1, 0xdf47); /* I2S Clock Configuration Register 1 */
//NUM[7:0]
SFRX(I2SCLKF2, 0xdf48); /* I2S Clock Configuration Register 2 */
// DENOM[8], NUM[14:8]
#define DENOM8_MSK  0x80
#define DENOM8_SFT  7
#define NUM_MSK     0x7f

/*** USB Registers ***/
SFRX(USBADDR, 0xde00);  /* Function Address */
// when the address becomes effective.
#define UPDATE      0x80
// Device address
#define USBADDR_MSK 0x7f

SFRX(USBPOW, 0xde01);   /* Power/Control Register */
// the USB controller will send zero length data packets
#define ISO_WAIT_SOF    0x80
// During reset signaling, this bit is set to1
#define RST             0x08
// Drive resume signaling for remote wakeup.
#define RESUME          0x04
// Suspend mode entered.
#define SUSPEND         0x02
// Suspend Enable.
#define SUSPEND_EN      0x01

SFRX(USBIIE, 0xde07);   /* IN Endpoints and EP0 Interrupt Enable Mask */
// IN endpoint 0-5 interrupt enable
#define INEP5IE 0x20
#define INEP4IE 0x10
#define INEP3IE 0x08
#define INEP2IE 0x04
#define INEP1IE 0x02
#define INEP0IE 0x01
SFRX(USBIIF, 0xde02);   /* IN Endpoints and EP0 Interrupt Flags */
// Interrupt flag for IN endpoint 0-5. Cleared by HW when read
#define INEP5IF 0x20
#define INEP4IF 0x10
#define INEP3IF 0x08
#define INEP2IF 0x04
#define INEP1IF 0x02
#define INEP0IF 0x01
SFRX(USBOIE, 0xde09);   /* Out Endpoints Interrupt Enable Mask */
// OUT endpoint 1-5 interrupt enable
#define OUTEP5IE 0x20
#define OUTEP4IE 0x10
#define OUTEP3IE 0x08
#define OUTEP2IE 0x04
#define OUTEP1IE 0x02
SFRX(USBOIF, 0xde04);   /* OUT Endpoints Interrupt Flags */
// Interrupt flag for OUT endpoint 1-5. Cleared by HW when read
#define OUTEP5IF 0x20
#define OUTEP4IF 0x10
#define OUTEP3IF 0x08
#define OUTEP2IF 0x04
#define OUTEP1IF 0x02
SFRX(USBCIE, 0xde0b);   /* Common USB Interrupt Enable Mask */
// Start-Of-Frame interrupt enable.
#define SOFIE       0x08
// Reset interrupt enable.
#define RSTIE       0x04
// Resume interrupt enable.
#define RESUMEIE    0x02
// Suspend interrupt enable.
#define SUSPENDIE   0x01
SFRX(USBCIF, 0xde06);   /* Common USB Interrupt Flags */
// Start-Of-Frame interrupt flag.
#define SOFIF       0x08
// Reset interrupt flag.
#define RSTIF       0x04
// Resume interrupt flag
#define RESUMEIF    0x02
// Suspend interrupt flag.
#define SUSPENDIF   0x01

SFRX(USBFRMH, 0xde0d);  /* Current Frame Number (High byte) */
SFRX(USBFRML, 0xde0c);  /* Current Frame Number (Low byte) */

SFRX(USBINDEX, 0xde0e); /* Selects current endpoint. */
// Endpoint selected. Must be set to value in the range 0 - 5
#define USBINDEX_MSK    0x07

SFRX(USBMAXI, 0xde10);  /* Max. packet size for IN endpoint */

SFRX(USBCS0, 0xde11);   /* EP0 Control and Status (USBINDEX = 0) */
// Set this bit to 1 to de-assert the SETUP_END bit of this register.
#define CLR_SETUP_END   0x80
// Set this bit to 1 to de-assert the OUTPKT_RDY bit of this register.
#define CLR_OUTPKT_RDY  0x40
// Set this bit to 1 to terminate the current transaction.
#define SEND_STALL0     0x20
// if the control transfer ends due to a premature end of control transfer.
#define SETUP_END       0x10
// the end of a data transfer
#define DATA_END        0x08
// This bit is set when a STALL handshake has been sent.
#define SENT_STALL0     0x04
// Set this bit when a data packet has been loaded into the EP0 FIFO
#define INPKT_RDY0      0x02
// Data packet received.
#define OUTPKT_RDY      0x01
SFRX(USBCSIL, 0xde11);  /* IN EP{1 - 5} Control and Status High */
// Setting this bit will reset the data toggle to 0.
#define CLR_DATA_TOG    0x40
// This bit is set when a STALL handshake has been sent.
#define SENT_STALL      0x20
// reply with a STALL handshake when receiving IN tokens.
#define SEND_STALL      0x10
// Set to 1 to flush next packet that is ready to transfer from the IIN FIFO.
#define FLUSH_PACKET    0x08
// if an IN token is received when INPKT_RDY=0
#define UNDERRUN        0x04
// This bit is 1 when there is at least one packet in the IN FIFO.
#define PKT_PRESENT     0x02
// Set this bit when a data packet has been loaded into the IN FIFO
#define INPKT_RDY       0x01

SFRX(USBCSIH, 0xde12);  /* IN EP{1 - 5} Control and Status High */
// the USBCSIL.INPKT_RDY bit is automatically asserted
#define AUTOSET         0x80
// Selects IN endpoint type
#define ISO             0x40
// force the IN endpoint data toggle to switch
#define FORCE_DATA_TOG  0x08
// Double buffering enable (IN FIFO)
#define IN_DBL_BUF      0x01

SFRX(USBMAXO, 0xde13);  /* Max. packet size for OUT endpoint */
// Max pack size in units of 8 bytes for OUT endpoint selected by USBINDEX.

SFRX(USBCSOL, 0xde14);  /* OUT EP{1 - 5} Control and Status Low */
// Setting this bit will reset the data toggle to 0.
#define OUT_CLR_DATA_TOG    0x80
// This bit is set when a STALL handshake has been sent.
#define OUT_SENT_STALL      0x40
// reply with a STALL handshake when receiving IN tokens.
#define OUT_SEND_STALL      0x20
// Set to 1 to flush next packet that is ready to transfer from the IIN FIFO.
#define OUT_FLUSH_PACKET    0x10
// is set if there is a CRC or bit-stuff error in the packet received.
#define DATA_ERROR          0x08
// if an IN token is received when INPKT_RDY=0
#define OVERRUN             0x04
// This bit is 1 when there is at least one packet in the IN FIFO.
#define FIFO_FULL           0x02
// Set this bit when a data packet has been loaded into the IN FIFO
#define OUTPKT_RDY          0x01
SFRX(USBCSOH, 0xde15);  /* OUT EP{1 - 5} Control and Status High */
// USBCSOL.OUTPKT_RDY bit is automatically cleared when a data packet of max
#define AUTOCLEAR   0x80
// Selects OUT endpoint type
#define OUT_ISO     0x40
// Double buffering enable (OUT FIFO)
#define OUT_DBL_BUF 0x01

SFRX(USBCNT0, 0xde16);  /* Number of received bytes in EP0FIFO(USBINDEX=0) */
SFRX(USBCNTL, 0xde16);  /* Number of Bytes in EP{1 - 5} OUT FIFO Low */
SFRX(USBCNTH, 0xde17);  /* Number of bytes in OUT FIFO High */

SFRX(USBF0, 0xde20);    /* Endpoint 0 FIFO */
SFRX(USBF1, 0xde22);    /* Endpoint 1 FIFO */
SFRX(USBF2, 0xde24);    /* Endpoint 2 FIFOh */
SFRX(USBF3, 0xde26);    /* Endpoint 3 FIFO */
SFRX(USBF4, 0xde28);    /* Endpoint 4 FIFO */
SFRX(USBF5, 0xde2a);    /* Endpoint 5 FIFO */

/*** Radio Registers ***/
SFRX(IOCFG2,0xdf2f);    /* Radio test signal configuration (P1_7) */
// Invert output, i.e. select active low (1) / high (0)
#define GDO2_INV        0x40
// Debug output on P1_7 pin.
#define GDO2_CFG_MSK    0x3f
SFRX(IOCFG1,0xdf30);    /* Radio test signal configuration (P1_6) */
// Drive strength control for I/O pins in output mode.
#define GDO_DS          0x80
// Invert output
#define GDO1_INV        0x40
// Debug output on P1_6 pin.
#define GDO1_CFG_MSK    0x3f
SFRX(IOCFG0,0xdf31);    /* Radio test signal configuration (P1_5) */
// Invert output, i.e. select active low (1) / high (0)
#define GDO0_INV        0x40
// Debug output on P1_5 pin.
#define GDO0_CFG_MSK    0x3f

SFRX(SYNC1, 0xdf00);    /* Sync word, high byte */
SFRX(SYNC0, 0xdf01);    /* Sync word, low byte */

SFRX(PKTLEN,0xdf02);    /* Packet length */
SFRX(ADDR,0xdf05);      /* Device address */
SFRX(CHANNR,0xdf06);    /* Channel number */

SFRX(PKTCTRL1,0xdf03);  /* Packet automation control */
// Preamble quality estimator threshold.
#define PQT_SFT 5
#define PQT_MSK 0xe0
// two status bytes will be appended to the payload of the packet.
#define APPEND_STATUS
// Controls address check configuration of received packages.
#define ADR_CHK_NO              0x00
#define ADR_CHK_NO_BROADCAST    0x01
#define ADR_CHK_0BROADCAST      0x02
#define ADR_CHK_0FFBROADCAST    0x03
SFRX(PKTCTRL0,0xdf04);  /* Packet automation control */
// Whitening enable.
#define WHITE_DATA          0x40
// Packet format of RX and TX data
#define PKT_FORMAT_NORMAL   0x00
#define PKT_FORMAT_RANDOM   0x20
// CRC calculation in TX and CRC check in RX enable
#define CRC_EN              0x04
// Packet Length Configuration
#define LENGTH_CONFIG_FIXED     0x00
#define LENGTH_CONFIG_VARIABLE  0x01

SFRX(FSCTRL1,0xdf07);   /* Frequency synthesizer control */
#define FREQ_IF(f)  ((f/(F_REF/1024))&0x1f)
SFRX(FSCTRL0,0xdf08);   /* Frequency synthesizer control */
#define FREQOFF(f)      ((f/(F_REF/16384))&0x7f)
#define FREQOFFM(f)     ((f/(F_REF/16384))&0x7f + 0x80)

SFRX(FREQ2,0xdf09);     /* Frequency control word, high byte */
SFRX(FREQ1,0xdf0a);     /* Frequency control word, middle byte */
SFRX(FREQ0,0xdf0b);     /* Frequency control word, low byte */

SFRX(MDMCFG4,0xdf0c);   /* Modem configuration */
SFRX(MDMCFG3,0xdf0d);   /* Modem configuration */
SFRX(MDMCFG2,0xdf0e);   /* Modem configuration */
SFRX(MDMCFG1,0xdf0f);   /* Modem configuration */
SFRX(MDMCFG0,0xdf10);   /* Modem configuration */

SFRX(DEVIATN,0xdf11);   /* Modem deviation setting */

SFRX(MCSM2,0xdf12);     /* Main Radio Control State Machine configuration */
// Direct RX termination based on RSSI measurement (carrier sense)
#define RX_TIME_RSSI    0x10
// estimation of sync word
#define RX_TIME_QUAL    0x08
// Timeout for sync word search in RX.
#define RX_TIME_MSK     0x07
#define RX_TIME_END     0x07
SFRX(MCSM1,0xdf13);     /* Main Radio Control State Machine configuration */
// Selects CCA_MODE; Reflected in CCA signal
#define CCA_MODE_ALWAYS         0x00
#define CCA_MODE_RSSI           0x10
#define CCA_MODE_PACKET         0x20
#define CCA_MODE_RSSI_PACKET    0x30
// Select what should happen (next state) when a packet has been received
#define RXOFF_MODE_IDLE         0x00
#define RXOFF_MODE_FSTXON       0x04
#define RXOFF_MODE_TX           0x08
#define RXOFF_MODE_RX           0x0c
// Select what should happen (next state) when a packet has been sent (TX)
#define TXOFF_MODE_IDLE         0x00
#define TXOFF_MODE_FSTXON       0x01
#define TXOFF_MODE_TX           0x02
#define TXOFF_MODE_RX           0x03
SFRX(MCSM0,0xdf14);     /* Main Radio Control State Machine configuration */
// Select calibration mode (when to calibrate)
#define FS_AUTOCAL_NEVER        0x00
#define FS_AUTOCAL_FROM_IDLE    0x10
#define FS_AUTOCAL_FROM_RXTX    0x20
#define FS_AUTOCAL_EVERY_4TH    0x30
// Sets RX attenuation.
#define CLOSE_IN_RX0DB          0x00
#define CLOSE_IN_RX6DB          0x01
#define CLOSE_IN_RX12DB         0x02
#define CLOSE_IN_RX18DB         0x03

SFRX(FOCCFG,0xdf15);    /* Frequency Offset Compensation configuration */
// demodulator freezes the frequency offset compensation
#define FOC_BS_CS_GATE      0x20
// frequency compensation loop gain to be used before a sync word is detected.
#define FOC_PRE_K           0x00
#define FOC_PRE_2K          0x08
#define FOC_PRE_3K          0x10
#define FOC_PRE_4K          0x18
// frequency compensation loop gain to be used after a sync word is detected.
#define FOC_POST_K          0x04
// The saturation point for the frequency offset compensation algorithm:
#define FOC_LIMIT_NO        0x00
#define FOC_LIMIT_BW_DIV9   0x01
#define FOC_LIMIT_BW_DIV4   0x02
#define FOC_LIMIT_BW_DIV2   0x03

SFRX(BSCFG,0xdf16);     /* Bit Synchronization configuration */
// clock recovery feedback loop integral gain to be used before a sync word
#define BS_PRE_KI   0x00
#define BS_PRE_2KI  0x40
#define BS_PRE_3KI  0x80
#define BS_PRE_4KI  0xc0
// clock recovery feedback loop proportional gain to be used before a sync
#define BS_PRE_KP   0x00
#define BS_PRE_2KP  0x10
#define BS_PRE_3KP  0x20
#define BS_PRE_4KP  0x30
// clock recovery feedback loop integral gain to be used after a sync word
#define BS_POST_KI  0x08
// clock recovery feedback loop proportional gain to be used after a sync word
#define BS_POST_KP  0x04
// The saturation point for the data rate offset compensation algorithm
#define BS_LIMIT0   0x00
#define BS_LIMIT3P  0x01
#define BS_LIMIT6P  0x02
#define BS_LIMIT12P 0x03

SFRX(AGCCTRL2,0xdf17);  /* AGC control */
// Reduces the maximum allowable DVGA gain.
#define MAX_DVGA_GAIN_ALL       0x00
#define MAX_DVGA_GAIN_WO_HIGH   0x40
#define MAX_DVGA_GAIN_WO_2HIGH  0x80
#define MAX_DVGA_GAIN_WO_3HIGH  0xc0
// Sets the maximum allowable LNA + LNA 2 gain relative to the max.
#define MAX_LNA_GAIN_MAX        0x00
#define MAX_LNA_GAIN_2P6DB      0x08
#define MAX_LNA_GAIN_6P1DB      0x10
#define MAX_LNA_GAIN_7P4DB      0x18
#define MAX_LNA_GAIN_9P2DB      0x20
#define MAX_LNA_GAIN_11P5DB     0x28
#define MAX_LNA_GAIN_14P6DB     0x30
#define MAX_LNA_GAIN_17P1DB     0x38
// target value for the averaged amplitude from the digital channel filter
#define MAGN_TARGET24DB         0x00
#define MAGN_TARGET27DB         0x01
#define MAGN_TARGET30DB         0x02
#define MAGN_TARGET33DB         0x03
#define MAGN_TARGET36DB         0x04
#define MAGN_TARGET38DB         0x05
#define MAGN_TARGET40DB         0x06
#define MAGN_TARGET42DB         0x07
SFRX(AGCCTRL1,0xdf18);  /* AGC control */
// Selects between two different strategies for LNA and LNA2 gain adjustment.
#define AGC_LNA_PRIORITY            0x40
// Sets the relative change threshold for asserting carrier sense
#define CARRIER_SENSE_REL_THR_DIS   0x00
#define CARRIER_SENSE_REL_THR6BD    0x10
#define CARRIER_SENSE_REL_THR10DB   0x20
#define CARRIER_SENSE_REL_THR14DB   0x30
// Sets the absolute RSSI threshold for asserting carrier sense
#define CARRIER_SENSE_ABS_THR_M8DB  0x08
#define CARRIER_SENSE_ABS_THR_M7DB  0x09
#define CARRIER_SENSE_ABS_THR_M6DB  0x0a
#define CARRIER_SENSE_ABS_THR_M5DB  0x0b
#define CARRIER_SENSE_ABS_THR_M4DB  0x0c
#define CARRIER_SENSE_ABS_THR_M3DB  0x0d
#define CARRIER_SENSE_ABS_THR_M2DB  0x0e
#define CARRIER_SENSE_ABS_THR_M1DB  0x0f
#define CARRIER_SENSE_ABS_THR_0DB   0x00
#define CARRIER_SENSE_ABS_THR_1DB   0x01
#define CARRIER_SENSE_ABS_THR_2DB   0x02
#define CARRIER_SENSE_ABS_THR_3DB   0x03
#define CARRIER_SENSE_ABS_THR_4DB   0x04
#define CARRIER_SENSE_ABS_THR_5DB   0x05
#define CARRIER_SENSE_ABS_THR_6DB   0x06
#define CARRIER_SENSE_ABS_THR_7DB   0x07
SFRX(AGCCTRL0,0xdf19);  /* AGC control */
// Sets the level of hysteresis on the magnitude deviation
#define HYST_LEVEL_NO               0x00
#define HYST_LEVEL_LOW              0x40
#define HYST_LEVEL_MEDIUM           0x80
#define HYST_LEVEL_LARGE            0xc0
// number of channel filter samples from a gain adjustment
#define WAIT_TIME8                  0x00
#define WAIT_TIME16                 0x10
#define WAIT_TIME24                 0x20
#define WAIT_TIME32                 0x30
// Controls when the AGC gain should be frozen.
#define AGC_FREEZE_NORMAL           0x00
#define AGC_FREEZE_SYNC_FROZEN      0x40
#define AGC_FREEZE_ANALOGUE_FROZEN  0x80
#define AGC_FREEZE_FROZE_BOTH       0xc0
// Sets the averaging length for the amplitude from the channel filter.
#define FILTER_LENGTH8              0x00
#define FILTER_LENGTH16             0x01
#define FILTER_LENGTH32             0x02
#define FILTER_LENGTH64             0x03

SFRX(FREND1,0xdf1a);    /* Front end RX configuration */
// Adjusts front-end LNA PTAT current output
#define LNA_CURRENT0            0x00
#define LNA_CURRENT1            0x40
#define LNA_CURRENT2            0x80
#define LNA_CURRENT3            0xc0
// Adjusts front-end PTAT outputs
#define LNA2MIX_CURRENT0        0x00
#define LNA2MIX_CURRENT1        0x10
#define LNA2MIX_CURRENT2        0x20
#define LNA2MIX_CURRENT3        0x30
// Adjusts current in RX LO buffer (LO input to mixer)
#define LODIV_BUF_CURRENT_RX0   0x00
#define LODIV_BUF_CURRENT_RX1   0x04
#define LODIV_BUF_CURRENT_RX2   0x08
#define LODIV_BUF_CURRENT_RX3   0x0c
// Adjusts current in mixer
#define MIX_CURRENT0            0x00
#define MIX_CURRENT1            0x01
#define MIX_CURRENT2            0x02
#define MIX_CURRENT3            0x03
SFRX(FREND0,0xdf1b);    /* Front end TX configuration */
// Adjusts current TX LO buffer (input to PA).
#define LODIV_BUF_CURRENT_TX0   0x00
#define LODIV_BUF_CURRENT_TX1   0x10
#define LODIV_BUF_CURRENT_TX2   0x20
#define LODIV_BUF_CURRENT_TX3   0x30
// Selects PA power setting.
#define PA_POWER_MSK            0x07

SFRX(FSCAL3,0xdf1c);    /* Frequency synthesizer calibration */
// Frequency synthesizer calibration configuration.
#define FSCAL3_0            0x00
#define FSCAL3_1            0x40
#define FSCAL3_2            0x80
#define FSCAL3_3            0xc0
// Disable charge pump calibration stage when 0
#define CHP_CURR_CAL_EN     0x20
#define CHP_CURR_CAL_EN_DIS 0x00
// Frequency synthesizer calibration result register.
#define FSCAL3_MSK          0x0f
SFRX(FSCAL2,0xdf1d);    /* Frequency synthesizer calibration */
// Select VCO
#define VCO_CORE_H_EN   0x20
// Frequency synthesizer calibration result register.
#define FSCAL2_MSK      0x1f
SFRX(FSCAL1,0xdf1e);    /* Frequency synthesizer calibration */
// Frequency synthesizer calibration result register.
#define FSCAL1_MSK      0x3f
SFRX(FSCAL0,0xdf1f);    /* Frequency synthesizer calibration */
// Frequency synthesizer calibration result register.
#define FSCAL0_MSK      0x7f

SFRX(TEST2,0xdf23);     /* Various Test Settings */
// At low data rates, the sensitivity can be improved by changing it to 0x81
#define TEST2_DEFAULT   0x88
#define TEST_LOW        0x81
SFRX(TEST1,0xdf24);     /* Various Test Settings */
// Always set this register to 0x31 when being in TX.
#define TEST1_DEFAULT   0x31
#define TEST_LOW_RX     0x35
SFRX(TEST0,0xdf25);     /* Various Test Settings */
// Enable VCO selection calibration stage when 1
#define VCO_SEL_CAL_EN  0x02

SFRX(PA_TABLE7,0xdf27); /* PA output power setting 7 */
SFRX(PA_TABLE6,0xdf28); /* PA output power setting 6 */
SFRX(PA_TABLE5,0xdf29); /* PA output power setting 5 */
SFRX(PA_TABLE4,0xdf2a); /* PA output power setting 4 */
SFRX(PA_TABLE3,0xdf2b); /* PA output power setting 3 */
SFRX(PA_TABLE2,0xdf2c); /* PA output power setting 2 */
SFRX(PA_TABLE1,0xdf2d); /* PA output power setting 1 */
SFRX(PA_TABLE0,0xdf2e); /* PA output power setting 0 */

SFRX(PARTNUM,0xdf36);   /* Chip ID[15:8] */
SFRX(VERSION,0xdf37);   /* Chip ID[7:0] */

SFRX(FREQEST,0xdf38);   /* Frequency Offset Estimate */
//The estimated frequency offset (2’s complement) of the carrier.
#define FREQOFF_EST(f)      ((f/(F_REF/16384))&0x7f)
#define FREQOFF_ESTM(f)     ((f/(F_REF/16384))&0x7f + 0x80)

SFRX(LQI,0xdf39);       /* Link Quality Indicator */
// The last CRC comparison matched.
#define CRC_OK      0x80
// estimates how easily a received signal can be demodulated
#define LQI_EST_MSK 0x7f
SFRX(RSSI,0xdf3a);      /* Received Signal Strength Indication */

SFRX(MARCSTATE,0xdf3b); /* Main Radio Control State */
// Main Radio Control FSM State
#define SLEEP           0x00
#define IDLE            0x01
#define VCOON_MC        0x03
#define REGON_MC        0x04
#define MANCAL          0x05
#define VCOON           0x06
#define REGON           0x07
#define STARTCAL        0x08
#define BWBOOST         0x09
#define FS_LOCK         0x0a
#define IFADCON         0x0b
#define ENDCAL          0x0d
#define RX              0x0c
#define RX_END          0x0e
#define RX_RST          0x0f
#define TXRX_SWITCH     0x10
#define RX_OVERFLOW     0x11
#define FSTXON          0x12
#define TX              0x13
#define TX_END          0x14
#define RXTX_SWITCH     0x15
#define TX_UNDERFLOW    0x16
SFRX(PKTSTATUS,0xdf3c); /* Packet status */
// The last CRC comparison matched.
#define CRC_OK
// Carrier sense
#define CS
// Preamble Quality reached
#define PQT_REACHED 0x20
// Channel is clear
#define CCA         0x10
// Asserted when sync word has been sent / received
#define SFD         0x08

SFRX(VCO_VC_DAC,0xdf3d);/* PLL calibration current */
// Status register for test only.

#endif
