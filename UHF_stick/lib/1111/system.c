/*
 *  CC1110 USB UHF stick. System functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system.h"

void interruptEnable(uint8_t vect, uint8_t level)
{
    uint8_t ipg = 0;
    // a table for interrut enable and priority bits choosing
    switch(vect) {
        case RFTXRX_INT:
            IEN0 |= RFTXRXIE;
            ipg = 0;
            break;
        case ADC_INT:
            IEN0 |= ADCIE;
            ipg = 1;
            break;
        case URX0_INT:
            IEN0 |= URX0IE;
            ipg = 2;
            break;
        case URX1_INT:
            IEN0 |= URX1IE;
            ipg = 3;
            break;
        case ENC_INT:
            IEN0 |= ENCIE;
            ipg = 4;
            break;
        case ST_INT:
            IEN0 |= STIE;
            ipg = 5;
            break;
        case P2INT_INT:
            IEN2 |= P2IE;
            ipg = 1;
            break;
        case UTX0_INT:
            IEN2 |= UTX0IE;
            ipg = 2;
            break;
        case DMA_INT:
            IEN1 |= DMAIE;
            ipg = 0;
            break;
        case T1_INT:
            IEN1 |= T1IE;
            ipg = 1;
            break;
        case T2_INT:
            IEN1 |= T2IE;
            ipg = 2;
            break;
        case T3_INT:
            IEN1 |= T3IE;
            ipg = 3;
            break;
        case T4_INT:
            IEN1 |= T4IE;
            ipg = 4;
            break;
        case P0INT_INT:
            IEN1 |= P0IE;
            ipg = 5;
            break;
        case UTX1_INT:
            IEN2 |= UTX1IE;
            ipg = 3;
            break;
        case P1INT_INT:
            IEN2 |= P1IE;
            ipg = 4;
            break;
        case RF_INT:
            IEN2 |= RFIE;
            ipg = 0;
            break;
        case WDT_INT:
            IEN2 |= WDTIE;
            ipg = 5;
            break;
    }
    // clear and set the priority bits
    IP1 &= ~IP1_IPGx(ipg);
    IP0 &= ~IP0_IPGx(ipg);
    if(level>1) {
        IP1 |= IP1_IPGx(ipg);
    }
    if((level == 1) || (level == 3)) {
        IP0 |= IP0_IPGx(ipg);
    }
}
