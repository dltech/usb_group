# usb_group
This repo is dedicated to the USB group, whose music is prohibited by law in russia.
It contains a typical examples of USB devices based on STM32 microcontroller. 
As for me it is the main competitor to the ST USB library from MCD team.

## Visuals
1. Keyboard \
![e](result/keyboard.png)
2. Optical mpuse \
![ee](mouse/pcb/mice_small.png)
![eee](result/mice_3d.png)
3. Phone exchange\
![d](phone_exchange/phone/phone_small.png)
![dd](phone_exchange/phone/phone_3d.png)
![ddd](phone_exchange/phone/phone_3d1.png)
4. 868 MHz USB-COM stick\
![c](UHF_stick/pcb/sch_small.png)
![c](UHF_stick/pcb/pcb3d.png)
![c](UHF_stick/pcb_1111/sch_small.png)
![c](UHF_stick/pcb_1111/pcb_3d.png)
5. Ti PCM1754 USB sound card\
![c](soundcard/pcb/sch_small.png)
![c](soundcard/pcb/pcb3d1.png)
## Installation
A PCB in kicad, a firmware in hex. Board can be made at home, for uploading firmware you need st-link stick.
All nesessary files are in result directory (A4 pcb templates in svg, hex and binary).

## Usage
If you would try to fork any device from the list, you will have to see it every day.

## Roadmap
plan is huge\
*optical mouse\
*ambilight\
*OV2640 webcam\
*CC1110 ISM USB-COM stick\
*ti pcm sound card

## License
nongnu

## Project status
not comleted yet.\
*keyboard sch ok\
*mouse sch ok\
*mouse pcb ok\
*phone exchange sch\pcb ok\
*usb 868 2 sch/pcb ok
