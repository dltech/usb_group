#ifndef H_ADNS2610_REGS
#define H_ADNS2610_REGS
/*
 * ADNS-2610 Optical Mouse Sensor. Register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define CONFIGURATION   0x00 /* Reset, Power Down, Forced Awake, etc */
// Reset, 1 = Reset the part
#define RESET       0x80
// Power down, 1 = power down all analog circuitry
#define PWRDOWN     0x40
// Forced Awake Mode, 0 = Normal, fall asleep after one s no movement (1500 frames/s)
#define AWAKE_MODE  0x01

#define STATUS          0x01 /* Product ID, Mouse state of Asleep or Awake */
// Product ID (000 for ADNS-2610)
#define ID_MSK      0xe0
#define ID_SFT      5
#define ID_GET(n)   ((ID_SFT>>n)&0x7)
// Mouse State, 0 = Asleep, 1 = Awake
#define AWAKE       0x01

#define DELTA_Y         0x02 /* Y Movement */
// Y movement is counted since last report. Absolute value is determined by resolution.
#define DELTA_X         0x03 /* X Movement */
// X movement is counted since last report. Absolute value is determined by resolution.

#define SQUAL           0x04 /* Measure of the number of features visible by the sensor */
// Surface QUALity is a measure of the num of features vis by the sensor in the frame.

#define MAXIMUM_PIXEL   0x05 /* Maximum Pixel value in current frame. */
// Maximum Pixel value in current frame. Minimum value = 0, maximum value = 63.
#define MINIMUM_PIXEL   0x06 /* Minimum Pixel value in current frame. */
// Minimum Pixel value in current frame. Minimum value = 0, maximum value = 63.
#define MP_MSK  0x3f

#define PIXEL_SUM       0x07 /*  This register is used to find the average pixel value. */
// It reports the up 8b of a 15b uint, which sums all 324 pixels in the current frame

#define PIXEL_DATA      0x08 /* Actual picture of surface */
// Start of Frame, 1 = Current pixel is number 1, start of frame
#define SOF     0x80
// Six bit pixel data
#define PD_MSK  0x3f

#define SHUTTER_UPPER   0x09 /* Shutter time in clock cycles MSB */
#define SHUTTER_LOWER   0x0a /* Shutter time in clock cycles LSB */
// The sensor adj the shut to keep the average and max pixel val within normal op ranges.

#define INVERSE_PRODUCT 0x11 /* Inverse Product ID */
// Status information and type of mouse sensor
#define IP_MSK  0xf

#endif
